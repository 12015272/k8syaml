#!/bin/bash

set -uo pipefail

if [ ! -n "${PD_SERVERS_list}" ]; then
  echo "pd-server is null,please input it."
  exit 1
fi

POD_IP=$(hostname -i)

sed -i s/PD_SERVER/${PD_SERVERS_list}/g /ticdc_start_script.sh

sed -i s/ADV_IP/${POD_IP}/g /ticdc_start_script.sh

/bin/bash ticdc_start_script.sh

