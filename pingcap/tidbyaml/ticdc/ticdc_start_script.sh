#!/bin/bash

# This script is used to start tikv containers in kubernetes cluster

set -uo pipefail

ARGS="--pd=http://PD_SERVER \
--log-file=/data/ticdc.log \
--advertise-addr=ADV_IP:8300 \
--addr=0.0.0.0:8300 \
--tz=Asia/Shanghai
"

echo "starting ticdc-server ..."
echo "/cdc server ${ARGS}"
exec /cdc server ${ARGS}
