# 编译ticdc

## 1、下载源码

需要用git方式下载，这样编译后的代码信息都能在`version`看到

```shell
git clone -b v4.0.5 https://github.com/pingcap/ticdc.git
```



如果是zip下载，需要手动指定环境变量

## 2、编译

```shell
make
```

输出结果在/bin/目录

## 3、`version`查看

```shell
[root]# ./cdc version
Release Version: v4.0.5
Git Commit Hash: 7b96199989397cf789b0f6adf91c51d885492241
Git Branch: HEAD
UTC Build Time: 2020-09-02 02:33:54
Go Version: go version go1.13.10 linux/amd64
```

