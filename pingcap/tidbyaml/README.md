# 使用说明

#### 介绍
启动tidb的方式


#### tikv安装教程

1.创建tikv配置文件和启动命令

configmap 添加多个文件及指定key
示例
  kubectl create configmap configmap-name --from-file=<my-key-name>=<path-to-file>

在跑
  kubectl create configmap yz-tikv-config --from-file=config-file=./tikv.toml --from-file=startup-script=./tikv_start_script.sh 

#### pd 安装教程

1、修改并确定pd的位置

nodeName: 指定pod要跑的机器IP
修改文件里的参数，确保每个pod 和机器ip 一一对应

2、创建pd配置文件

kubectl create configmap pd-cross-config --from-file=config-file=./pd.toml


#### tiflash 安装教程
1、pd 开启 Placement Rules 特性

默认情况下，Placement Rules 特性是关闭的。要开启这个特性，可以集群初始化以前设置 PD 配置文件
[replication] 
enable-placement-rules = true

如果是已经初始化过的集群，也可以通过 pd-ctl 进行在线开启：

pd-ctl config placement-rules enable

2、创建tiflash sts 和 cm ，添加 node selector
注意 sts 是自己写的，需要添加变量 PD_SERVERS_list，TIDB_SERVERS_list, 这2个list 可以用haproxy代理
hostname -i 要返回 主机ip，才能安装


#### grafana 安装配置

1、目标机器创建目录
mkdir -p provisioning/datasources
mkdir -p provisioning/dashboards
mkdir -p provisioning/notifiers

2、创建grafana配置文件
kubectl create configmap cross-grafana-config --from-file=config-file=./grafana.ini --from-file=ldap-file=./ldap.toml


#### prometheus 安装配置
1、创建rules configmap
kubectl create configmap prometheus-crosspdrules --from-file=./pd.rules.yml
kubectl create configmap prometheus-crosstikvrules --from-file=./tikv.rules.yml
kubectl create configmap prometheus-crosstidbrules --from-file=./tidb.rules.yml
kubectl create configmap prometheus-crossbinlogrules --from-file=./binlog.rules.yml

2、创建prometheus 配置文件
kubectl create configmap  prometheus-crossconfig --from-file=./prometheus.yml

3、添加reload功能
启动参数添加
--web.enable-lifecycle
发送post请求
 curl -XPOST http://prometheus.emar.com/-/reload

#### ticdc 安装配置

1、先编译或者下载cdc源码，生成cdc binary

2、确定node selector 和 pd server，修改sts