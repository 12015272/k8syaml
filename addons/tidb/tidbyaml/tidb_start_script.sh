#!/bin/bash

# This script is used to start tikv containers in kubernetes cluster

set -uo pipefail

ARGS="--store=tikv \
    --host=0.0.0.0 \
    --path=${PD_SERVERS} \
    --config=/etc/tidb/tidb.toml \
    --log-file=/logs/tidb.log
"

echo "starting tidb-server ..."
echo "/tidb-server ${ARGS}"
exec /tidb-server ${ARGS}
