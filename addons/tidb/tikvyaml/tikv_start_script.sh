#!/bin/bash

# This script is used to start tikv containers in kubernetes cluster

set -uo pipefail

if [ $? -eq 0 ] ; then
    localip=`/sbin/ifconfig |grep "inet addr"|grep -v docker|awk -F : '{print $2}' |awk '{print $1}'|grep -v "^172.17"|grep -v "^192.168"|grep -v "172.16"|grep -v "10.244"|head -n 1 `
else
    localip=`/sbin/ifconfig |grep "inet "|grep -v docker|awk '{print $2}'|awk '{print $1}'|grep -v ^172|grep -v ^192|grep -v ^127|grep -v 10.244`

fi

ARGS="--pd=${PD_SERVERS} \
--advertise-addr=${localip}:20160 \
--addr=0.0.0.0:20160 \
--status-addr=0.0.0.0:20180 \
--data-dir=/data/tikv \
--capacity=${CAPACITY} \
--config=/etc/tikv/tikv.toml
"

echo "starting tikv-server ..."
echo "/tikv-server ${ARGS}"
exec /tikv-server ${ARGS}
