#!/bin/sh

# This script is used to start pd containers in kubernetes cluster

set -uo pipefail

# Use HOSTNAME if POD_NAME is unset for backward compatibility.
POD_NAME=`hostname`

if [ $? -eq 0 ] ; then
    localip=`/sbin/ifconfig |grep "inet addr"|grep -v docker|awk -F : '{print $2}' |awk '{print $1}'|grep -v "^172.17"|grep -v "^192
.168"|grep -v "172.16"|grep -v "10.244"|head -n 1 `
else
    localip=`/sbin/ifconfig |grep "inet "|grep -v docker|awk '{print $2}'|awk '{print $1}'|grep -v ^172|grep -v ^192|grep -v ^127|gr
ep -v 10.244`

fi


ARGS="--data-dir=/data/pd \
--name=${POD_NAME} \
--peer-urls=http://0.0.0.0:2580 \
--advertise-peer-urls=http://${localip}:2580 \
--client-urls=http://0.0.0.0:2579 \
--advertise-client-urls=http://${localip}:2579 \
--config=/etc/pd/pd.toml \
--initial-cluster=emarsys105011=http://101.254.242.11:2580,host105045=http://101.254.242.45:2580,host105037=http://101.254.242.47:2580
"


echo "starting pd-server ..."
sleep $((RANDOM % 10))
echo "/pd-server ${ARGS}"
exec /pd-server ${ARGS}
