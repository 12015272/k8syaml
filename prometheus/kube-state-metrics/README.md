# 官方URL
https://github.com/kubernetes/kube-state-metrics

# 版本适配
| kube-state-metrics | **Kubernetes 1.15** | **Kubernetes 1.16** |  **Kubernetes 1.17** |  **Kubernetes 1.18** |  **Kubernetes 1.19** |
|--------------------|---------------------|---------------------|----------------------|----------------------|----------------------|
| **v1.8.0**         |         ✓           |         -           |          -           |          -           |          -           |
| **v1.9.7**         |         -           |         ✓           |          -           |          -           |          -           |
| **v2.0.0-alpha.1** |         -           |         -           |          ✓           |          ✓           |          ✓           |
| **master**         |         -           |         -           |          ✓           |          ✓           |          ✓           |
- `✓` Fully supported version range.
- `-` The Kubernetes cluster has features the client-go library can't use (additional API objects, deprecated APIs, etc).
